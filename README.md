# Duke University Theses/Dissertations Template

This repository contains a LaTeX template for Duke University Theses/Dissertations.
The Graduate School and OIT do not provide any LaTeX support beyond a template, and the [original template](https://duke.app.box.com/s/cjeljnoxgru393dkzvuyoxpbmoh5fhu9) distributed through the [website of The Graduate School](https://gradschool.duke.edu/academics/theses-and-dissertations/) is highly outdated (dates from 2011).
There is also an [Overleaf template for Duke University theses/dissertations](https://www.overleaf.com/latex/templates/template-for-duke-university-theses-slash-dissertations/kdnjzddhqtny) which is much better, but there are still some problems and the template still dates from 2016.
Hence, I decided to set up a [repository](https://gitlab.com/lriesebos/duke-theses-dissertations-template) with a LaTeX template starting from the Overleaf template and adding my fixes and suggestions.

The compiled PDF version of this template can be downloaded from the [CI/CD pipeline page](https://gitlab.com/lriesebos/duke-theses-dissertations-template/-/pipelines).

## Usage

A lot of students use [Overleaf](https://www.overleaf.com/) for creating LaTeX documents. One way to use this template is to download this repository as a zip file and create a new project in Overleaf choosing the "upload project" option. Large dissertations might not compile within the time limits of a free account and you might have to upgrade your account. Alternatively, you can compile the document locally or use your favorite tools/services to work with LaTeX documents.

## Common problems

References from the bib file do not process correctly.
This is often caused by the direct usage of UTF-8 special characters in the bib file. The [BibLaTeX package documentation](https://www.ctan.org/pkg/biblatex) has a section "specifying encodings" that suggests to use `\usepackage[utf8]{inputenc}` before the BibLaTeX `\usepackage` command for direct UTF-8 input in the bib file.
Alternatively, special characters in the bib file can be expressed with [escape codes](https://en.wikibooks.org/wiki/LaTeX/Special_Characters#Escaped_codes).

## Contributing

Feel free to fork this project and submit merge requests to this project.

## Authors and acknowledgment

Duke University Graduate School and LianTze Lim made the original template available as an Overleaf template.
Names of the original authors can be found in the [`dukedissertation.cls`](dukedissertation.cls) file.

## License

The sources were originally taken from the [Overleaf template for Duke University theses/dissertations](https://www.overleaf.com/latex/templates/template-for-duke-university-theses-slash-dissertations/kdnjzddhqtny) in October 2022. The license information of the source:

> Author: Duke University Graduate School (uploaded by LianTze Lim)
>
> License: Creative Commons CC BY 4.0
>
> Abstract: This template is provided by Duke's Graduate School to help eliminate common formatting errors (downloaded from here on 4 Feb 2016). Please refer to the specifications listed in the guide for thesis/dissertation submission. The manuscript must be a completed document, formatted correctly, with no sections left blank.

The [`dukedissertation.cls`](dukedissertation.cls) file contains the following license information:

```latex
%% This file may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either version 1.3
%% of this license or (at your option) any later version.
%% The latest version of this license is in
%%    http://www.latex-project.org/lppl.txt
%% and version 1.3 or later is part of all distributions of LaTeX
%% version 2003/12/01 or later.
```
